define([], function() {
	var Vector = function(x, y) {
			this.x = x;
			this.y = y;
			return this;
		},
		proto = Vector.prototype,
		pow = Math.pow,
		sqrt = Math.sqrt,
		round = Math.round,
		approxEqual;

	// Helper function
	approxEqual = function(lhs, rhs, digits) {
		var mult = pow(10, digits || 10);
		return (round(lhs * mult) / mult) === (round(rhs * mult) / mult);
	};

	// Functions on prototype
	proto.set = function(v, w) {
		var t = this;
		if (w !== undefined) {
			t.x = v;
			t.y = w;
		}
		else {
			t.x = v.x;
			t.y = v.y;
		}
		return t;
	};

	proto.clone = function() {
		return new Vector(this.x, this.y);
	};

	proto.equals = function(v, w) {
		if (w !== undefined) {
			return this.x === v && this.y === w;
		}
		return this.x === v.x && this.y === v.y;
	};

	proto.approxEquals = function(v, w, digits) {
		if (w !== undefined) {
			return approxEqual(this.x, v, digits) && approxEqual(this.y, w, digits);
		}
		return approxEqual(this.x, v.x, digits) && approxEqual(this.y, v.y, digits);
	};

	proto.round = function() {
		this.x = round(this.x);
		this.y = round(this.y);
		return this;
	};

	proto.add = function(v) {
		return new Vector(this.x + v.x, this.y + v.y);
	};
	
	proto.incrementBy = function(v) {
		this.x += v.x;
		this.y += v.y;
	};

	proto.subtract = function(v) {
		return new Vector(this.x - v.x, this.y - v.y);
	};
	
	proto.decrementBy = function(v) {
		this.x -= v.x;
		this.y -= v.y;
	};

	proto.scale = function(scalar) {
		return new Vector(this.x * scalar, this.y * scalar);
	};
	
	proto.scaleBy = function(scalar) {
		this.x *= scalar;
		this.y *= scalar;
	};

	proto.magnitude = function() {
		return sqrt(pow(this.x, 2) + pow(this.y, 2));
	};

	proto.magnitudeSquared = function() {
		return pow(this.x, 2) + pow(this.y, 2);
	};

	proto.distanceTo = function(v) {
		return sqrt(pow(v.x - this.x, 2) + pow(v.y - this.y, 2));
	};

	proto.distanceToSquared = function(v) {
		return pow(v.x - this.x, 2) + pow(v.y - this.y, 2);
	};

	proto.dotProduct = function(v) {
		return (this.x * v.x) + (this.y * v.y);
	};
	
	proto.normalize = function() {
		var magnitude = this.magnitude();
		this.x /= magnitude;
		this.y /= magnitude;
	};
	
	proto.negate = function() {
		this.x = -this.x;
		this.y = -this.y;
	};

	/**
	 * Returns the angle between the two given vectors
	 */
	proto.angleTo = function(p) {
		// http://stackoverflow.com/a/2150475/311207
		return Math.atan2(this.x * p.y - this.y * p.x, this.x * p.x + this.y * p.y);
	};

	/**
	 * Rotate a point about the origin by a given theta
	 */
	proto.rotate = function(theta) {
		return this.rotateCosSin(Math.cos(theta), Math.sin(theta));
	};

	/**
	 * Rotate a point by a given cos(0) and sin(0). In practice, this function
	 * is getting called when multiple points are getting transformed by the
	 * same theta (0). The calling code caches cos(0) and sin(0) and call this
	 * function instead of `rotate`, for better performance.
	 */
	proto.rotateCosSin = function(cosTheta, sinTheta) {
		// http://stackoverflow.com/a/3162657/311207
		return new Vector(cosTheta * this.x - sinTheta * this.y, sinTheta * this.x + cosTheta * this.y);
	};
	
	proto.isZero = function() {
		return this.x === 0 && this.y === 0;
	};

	// Static: unit vectors
	Vector.ORIGIN = new Vector(0, 0);
	Vector.X = new Vector(1, 0);
	Vector.Y = new Vector(0, 1);

	return Vector;
});