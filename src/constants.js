define([], function() {
	// GAME-WIDE "CONSTANTS"
	return {
	    CANVAS_WIDTH: 960,
	    CANVAS_HEIGHT: 640,
	    FPS: 30,
	    MAP_OFFSET: { x: 160, y: 40 } // To be replaced by a camera class if/when scrolling maps are implemented
	};
});