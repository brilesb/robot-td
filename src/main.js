require.config({
  	paths: {
    		'jquery': '../lib/jquery-2.0.3.min'
  	}
});

require([
    'jquery',
  	'constants',
  	'states/StateManager',
  	'states/TitleState'
], function($, CONSTANTS, StateManager, TitleState) {
	
    var stateManager = new StateManager(); // Manages game states

    // Wait for DOM readiness...
    $(function() {
        var ctx = document.getElementById("gameCanvas").getContext("2d");

        // Initalization
        //-------------------------------------------------------
        // Set inital state
        stateManager.pushState(new TitleState(stateManager));

        // Main Game Loop
        //-------------------------------------------------------
        setInterval(function () {

            // the current state manages what is going on...
            // so this pretty much all we need in the main loop
            // look to the update/draw functions of the states for
            // the real detail of what's happening
            stateManager.update();
            stateManager.draw(ctx, CONSTANTS.CANVAS_WIDTH, CONSTANTS.CANVAS_HEIGHT);

        }, 1000 / CONSTANTS.FPS);
    });
});