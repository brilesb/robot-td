define([], function() {
	/**
	 * Entity message, used to communicate between components and other entities 
	 */
	var Message = function(type, value) {
		this.type = type;
		this.value = value;
	};

	
	return Message;
});