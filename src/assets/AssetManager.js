define([
	'states/GameState',
	'maps/TileType',
	'maps/TileMap',
], function(GameState, TileType, TileMap) {
	
	/**
	 * Manages a group of assets (images, sounds, etc...) Currently only images is implemented.
	 */
	var AssetManager = function() {
		this.images = {};
	};

	/**
	 * Loads a list of images. NEEDS TO HANDLE ERRORS
	 * @param {Array} imgList	List of images to load. Expects an array of objects with "name" and "source" properties.
	 * @param {Function} onLoadComplete	Function to call when all images have been loaded.
	 * @param {Function} onImageLoaded	Function to call everytime a single image is loaded. Passes name of image loaded.
	 */
	AssetManager.prototype.loadImages = function (imgList, onLoadComplete, onImageLoaded) {
		var numLoaded = 0;
		
		for(var i = 0; i < imgList.length; i++) {
			var image = new Image();
			this.images[imgList[i].name] = image;
			image.onload = function() {
				numLoaded++;
				if(onImageLoaded) {
					onImageLoaded(name);
				}
				if(numLoaded >= imgList.length) {
					onLoadComplete();
				}
			};
			image.src = imgList[i].source;
		}
	};
	
	/**
	 * Clears all images from the asset manager 
	 */
	AssetManager.prototype.clearImages = function() {
		// any way to clear an object without just creating a new one?
		images = {};
	};

	return AssetManager;
});