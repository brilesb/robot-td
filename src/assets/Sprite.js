define([], function() {
	/**
	 * Sprite (image) object. Static, not animated.
	 */
	var Sprite = function(image, left, top, width, height) {
		this.image = image;
		// optional parameters
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
	};

	Sprite.prototype.draw = function(ctx, x, y, left, top, width, height) {
		// I don't like the way this works but I can't think of a better way to deal with optional params
		if(width && height) { // use passed in optional parameters
			ctx.drawImage(this.image, left, top, width, height, x, y, width, height);
		}
		else { // used stored values
			ctx.drawImage(this.image, this.left, this.top, this.width, this.height, x, y, this.width, this.height);
		}
		
	};
	
	Sprite.prototype.destroy = function() {
		this.image = null;
	};
	

	return Sprite;
});