define([
	'components/ComponentType',
	'geometry/vector'
], function(ComponentType, Vector) {

	/**
	 * Handles physics for a moving object. This class is simple and assumes constant speed, no acceleration/decceleration
	 */
	var MovingPhysics = function(speed) {
		this.speed = speed;
		this.moveVelocity = new Vector(0,0); // reduces object creation during update()
		this.entity = null;
	};
	
	MovingPhysics.prototype.type = ComponentType.PHYSICS; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	MovingPhysics.prototype.name = "MovingPhysics"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	MovingPhysics.prototype.update = function() {
		if(this.entity.debugMe) {
			this.entity.debugMe;
		}
		
		
		// super simple reducer... could be more complicated to deal with friction/air resistance or whatever
		// you'd need more complicated to simulate units driving over ice, etc...
		this.entity.velocity.set(0,0);
		
		// moveDirection is assumed to be normalized, don't want to do it twice
		this.moveVelocity.set(0,0);
		if(this.entity.moveDirection && !this.entity.moveDirection.isZero()) {
			// instant acceleration! this simulation is very simple
			this.moveVelocity.set(this.entity.moveDirection);
			this.moveVelocity.scaleBy(this.speed);
		}
		
		this.entity.velocity.incrementBy(this.moveVelocity);
		this.entity.position.incrementBy(this.entity.velocity);
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	MovingPhysics.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") ) {
			throw this.name + " Component requires the properties ('position')";
		}
		// check for movement related properties... if they don't exist just add them
		if (!this.entity.hasOwnProperty("velocity")) {
			this.entity.velocity = new Vector(0,0);
		}
		if (!this.entity.hasOwnProperty("moveDirection")) {
			this.entity.moveDirection = new Vector(0,0);
		}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	MovingPhysics.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	MovingPhysics.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return MovingPhysics;
});