define([
	'components/ComponentType',
	'Team',
	'geometry/vector'
], function(ComponentType, Team, Vector) {

	/**
	 * AI for Tower Entities. Determines target priority
	 */
	var TowerAI = function(range) {
		this.entity = null;
		this.range = range; // used for display purposes only
		this.rangeSqr = range * range;
	};
	
	TowerAI.prototype.type = ComponentType.AI; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	TowerAI.prototype.name = "TowerAI"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	TowerAI.prototype.update = function(ctx) {
		
		// pick a target
		var nearby = this.entity.manager.getEntitiesWithComponentInRange(ComponentType.INTERACTION, "Damageable", this.entity.position, this.rangeSqr);
		this.entity.target = null;
		// priority? blahhhh... just use the closest for now
		for (var i = 0; i < nearby.length; i++) {
			if(nearby[i].entity.team == Team.HOSTILE) {
				this.entity.target = nearby[i].entity;
				break;
			}
		}
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	TowerAI.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") || 
			!this.entity.hasOwnProperty("width") ||
			!this.entity.hasOwnProperty("height") ||
			!this.entity.hasOwnProperty("team")) {
				throw this.name + " Component requires the properties ('position','width','height','team')";
		}
		// things we might be missing but can just add (no prereq values needed)
		if(!this.entity.hasOwnProperty("target")) {
			this.entity.target = null;
		}
		if(!this.entity.hasOwnProperty("lookDirection")) {
			this.entity.lookDirection = new Vector(-1, 0); // everybody look left
		}
		if(!this.entity.hasOwnProperty("moveDirection")) {
			this.entity.moveDirection = new Vector(0, 0); // stop! vector time
		}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	TowerAI.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	TowerAI.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return TowerAI;
});