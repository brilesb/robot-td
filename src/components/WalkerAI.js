define([
	'components/ComponentType',
	'geometry/vector'
], function(ComponentType, Vector) {

	/**
	 * AI for Walking Entities.
	 */
	var WalkerAI = function() {
		this.entity = null;
	};
	
	WalkerAI.prototype.type = ComponentType.AI; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	WalkerAI.prototype.name = "WalkerAI"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	WalkerAI.prototype.EvaluatePathWait = 20; // evaulate current path every 20 frames
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	WalkerAI.prototype.update = function(ctx) {
		
		// look through our targets and see if we need to pick a new one
		
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	WalkerAI.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") || 
			!this.entity.hasOwnProperty("width") ||
			!this.entity.hasOwnProperty("height") ||
			!this.entity.hasOwnProperty("team")) {
				throw this.name + " Component requires the properties ('position','width','height','team')";
		}
		// things we might be missing but can just add (no prereq values needed)
		if(!this.entity.hasOwnProperty("target")) {
			this.entity.target = null;
		}
		if(!this.entity.hasOwnProperty("lookDirection")) {
			this.entity.lookDirection = new Vector(-1, 0); // everybody look left
		}
		if(!this.entity.hasOwnProperty("moveDirection")) {
			this.entity.moveDirection = new Vector(-1, 0); // stop! vector time
		}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	WalkerAI.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	WalkerAI.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return WalkerAI;
});