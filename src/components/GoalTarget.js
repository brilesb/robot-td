define([
	'components/ComponentType'
], function(ComponentType) {

	/**
	 * This entity is a goal target. If all the goal targets are destroyed the player loses
	 */
	var GoalTarget = function() {
		this.entity = null;
	};
	
	GoalTarget.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	GoalTarget.prototype.name = "GoalTarget"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	GoalTarget.prototype.update = function(ctx) {
	
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	GoalTarget.prototype.start = function() {
		
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	GoalTarget.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	GoalTarget.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return GoalTarget;
});