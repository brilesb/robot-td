define([], function() {
	
	return {
		RENDERING: "RENDERING",
		PHYSICS: "PHYSICS",
		POST_PHYSICS: "POST_PHYSICS", // Physics post processing
		AI: "AI",
		INTERACTION: "INTERACTION",
		STATIC: "STATIC"	// Never gets update()ed
	};
});