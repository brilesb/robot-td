define([
	'components/ComponentType',
	'components/DebugRender',
	'components/Damaging',
	'components/MovingPhysics',
	'geometry/vector'
], function(ComponentType, DebugRender, Damaging, MovingPhysics, Vector) {

	/**
	 * Emits projectiles. Should be called ProjectileEmitter but that is too long.
	 */
	var ProjEmitter = function(entityManager, timeBetweenShots, createProj) {
		this.entityManager = entityManager;
		this.timeBetweenShots = timeBetweenShots;
		this.createProj = createProj;
		this.timer = timeBetweenShots; // shot timer
		this.entity = null;
	};
	
	ProjEmitter.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	ProjEmitter.prototype.name = "ProjEmitter"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	ProjEmitter.prototype.update = function(ctx) {
		this.timer--;
		
		// check if we have a target, and it's time to shoot
		if(this.timer <= 0 && this.entity.target)
		{
			if(this.createProj) {
				var projEntity = this.createProj();
				projEntity.position.set(this.entity.position);
				
				// determine looking at target vector
				var targetPos = new Vector().set(this.entity.target.position);
				// take into account the target's movement
				if(this.entity.target.moveVelocity) {
					// this is kinda dumb, but I'm not feeling motivated today
					targetPos.incrementBy(this.entity.target.moveVelocity);
				}
				
				projEntity.moveDirection.set(targetPos);
				projEntity.moveDirection.decrementBy(this.entity.position);
				projEntity.moveDirection.normalize();
				
				
				this.entity.manager.addEntity(projEntity);
			}
			this.timer = this.timeBetweenShots;
		}
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	ProjEmitter.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") || 
			!this.entity.hasOwnProperty("width") ||
			!this.entity.hasOwnProperty("height") ||
			!this.entity.hasOwnProperty("team")) {
				throw this.name + " Component requires the properties ('position','width','height','team')";
		}
		// things we might be missing but can just add (no prereq values needed)
		if(!this.entity.hasOwnProperty("target")) {
			this.entity.target = null;
		}
		if(!this.entity.hasOwnProperty("lookDirection")) {
			this.entity.lookDirection = new Vector(-1, 0); // everybody look left
		}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	ProjEmitter.prototype.shutdown = function() {
		
	};
	
	ProjEmitter.prototype.emitProjectile = function() {
		var entity = new Entity();
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	ProjEmitter.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return ProjEmitter;
});