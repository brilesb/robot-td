define([
	'components/ComponentType',
	'messages/Message',
	'messages/MessageType'
], function(ComponentType, Message, MessageType) {

	/**
	 * Damages the targeted entity at a specific rate
	 */
	var InstantDamager = function(timeBetweenDamage, damageAmount) {
		this.timeBetweenDamage = timeBetweenDamage;
		this.damageAmount = damageAmount;
		this.timer = timeBetweenDamage; // damage timer
		this.entity = null;
	};
	
	InstantDamager.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	InstantDamager.prototype.name = "InstantDamager"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	InstantDamager.prototype.update = function(ctx) {
		this.timer--;
		
		// check if we have a target, and it's time to damage
		if(this.timer <= 0 && this.entity.target)
		{
			var msg = new Message(MessageType.DAMAGE, this.damageAmount);
			this.entity.target.sendMessage(msg);
			this.timer = this.timeBetweenDamage;
		}
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	InstantDamager.prototype.start = function() {
		// verify that entity has required properties
		
		// things we might be missing but can just add (no prereq values needed)
		if(!this.entity.hasOwnProperty("target")) {
			this.entity.target = null;
		}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	InstantDamager.prototype.shutdown = function() {
		
	};
	
	InstantDamager.prototype.emitProjectile = function() {
		var entity = new Entity();
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	InstantDamager.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return InstantDamager;
});