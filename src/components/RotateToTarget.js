define([
	'components/ComponentType',
	'geometry/vector'
], function(ComponentType, Vector) {

	/**
	 * Rotates an entities lookDirection towards it's target
	 */
	var RotateToTarget = function(rotateSpeed) {
		this.rotateSpeed = rotateSpeed;
		this.currentTarget = null;
		this.entity = null;
		this.lookAtTarget = new Vector(0, 0);
		this.targetPosition = new Vector(0, 0);
	};
	
	RotateToTarget.prototype.type = ComponentType.PHYSICS; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	RotateToTarget.prototype.name = "RotateToTarget"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	RotateToTarget.prototype.update = function(ctx) {
		
		if(this.entity.target) {
			// check to see if the target moved
			if(!this.entity.target.position.equals(this.targetPosition)) {
				this.targetPosition.set(this.entity.target.position);
				
				// determine looking at target vector
				this.lookAtTarget.set(this.entity.target);
				this.lookAtTarget.decrementBy(this.entity.position);
				this.lookAtTarget.normalize();
				
				// determine turn direction
			}
			// keep on turning
			// if close only move amount needed and then stop turning
			
			// screw all that for now, just move instantly
			this.entity.lookDirection.set(this.lookAtTarget);
		}
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	RotateToTarget.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position")) {
				throw this.name + " Component requires the property ('position')";
		}
		// things we might be missing but can just add (no prereq values needed)
		if(!this.entity.hasOwnProperty("target")) {
			this.entity.target = null;
		}
		if(!this.entity.hasOwnProperty("lookDirection")) {
			this.entity.lookDirection = new Vector(-1, 0); // everybody look left
		}
			
		this.currentTarget = this.entity.target;
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	RotateToTarget.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	RotateToTarget.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return RotateToTarget;
});