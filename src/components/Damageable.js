define([
	'components/ComponentType',
	'messages/MessageType'
], function(ComponentType, MessageType) {

	/**
	 * Makes an entity able to receive damage 
	 */
	var Damageable = function(maxHealth) {
		this.maxHealth = maxHealth;
		this.health = maxHealth;
		this.entity = null;
	};
	
	Damageable.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	Damageable.prototype.name = "Damageable"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	Damageable.prototype.update = function(ctx) {
		
		
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	Damageable.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") || 
			!this.entity.hasOwnProperty("width") ||
			!this.entity.hasOwnProperty("height") ||
			!this.entity.hasOwnProperty("team")) {
				throw this.name + " Component requires the properties ('position','width','height','team')";
			}
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	Damageable.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	Damageable.prototype.processMessage = function(message) {
		if(message != null)
		{
			alert("Message " + message.type);
			if(message.type === MessageType.DAMAGE)
			{
				// the value is damage
				this.health -= message.value;
				if(this.health <= 0)
				{
					// he's dead jim
					//this.entity.sendMessage(new Message(MessageType.DEATH,null));
					this.entity.destroy();
				}
			}
		}
	};
	
	return Damageable;
});