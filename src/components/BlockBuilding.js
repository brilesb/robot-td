define([
	'components/ComponentType'
], function(ComponentType) {
	
	/**
	 * Entities this component is attached to will block building at their location 
	 */
	var BlockBuilding = function() {
		
	};
	
	BlockBuilding.prototype.type = ComponentType.STATIC;
	BlockBuilding.prototype.name = "BlockBuilding";
	
	BlockBuilding.prototype.start = function() {
		// do nothing
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	BlockBuilding.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return BlockBuilding;
});
