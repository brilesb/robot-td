define([
	'components/ComponentType',
	'messages/MessageType'
], function(ComponentType,MessageType) {

	/**
	 * Damages any
	 */
	var Damaging = function(damage) {
		this.damage = damage;
		this.entity = null;
	};
	
	Damaging.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	Damaging.prototype.name = "Damaging"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	Damaging.prototype.update = function(ctx) {
		
		// what do we do here... I dunno ?
			
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	Damaging.prototype.start = function() {
		
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	Damaging.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	Damaging.prototype.processMessage = function(message) {
		
	};
	
	return Damaging;
});