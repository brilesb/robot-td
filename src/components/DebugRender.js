define([
	'components/ComponentType'
], function(ComponentType) {

	/**
	 * Renders a simple colored rectangle. Used for debugging and development. 
	 */
	var DebugRender = function(color, mapOffset) {
		this.color = color;
		this.mapOffset = mapOffset;
		this.entity = null;
		this.halfWidth = 0;
		this.halfHeight = 0;
	};
	
	DebugRender.prototype.type = ComponentType.RENDERING; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	DebugRender.prototype.name = "DebugRender"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	DebugRender.prototype.update = function(ctx) {
		// assumes the existence of the following entity properties
		// x, y, width, height
		if (this.entity != null) {
			ctx.fillStyle = this.color;
			ctx.fillRect(this.entity.position.x + this.mapOffset.x - this.halfWidth, this.entity.position.y + this.mapOffset.y - this.halfHeight, this.entity.width, this.entity.height);
		}	
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	DebugRender.prototype.start = function() {
		// verify that entity has required properties
		if( !this.entity.hasOwnProperty("position") || 
			!this.entity.hasOwnProperty("width") ||
			!this.entity.hasOwnProperty("height")) {
				throw this.name + " Component requires the properties ('position','width','height')";
			}
		// setup variables so we don't have to do so much bloody division
		this.halfWidth = this.entity.width / 2;
		this.halfHeight = this.entity.height / 2;
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	DebugRender.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	DebugRender.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return DebugRender;
});