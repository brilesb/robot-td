define([
	'components/ComponentType'
], function(ComponentType) {

	/**
	 * Makes an entity able to receive damage 
	 */
	var DeathCountdown = function(countdown) {
		this.countdown = countdown;
		this.entity = null;
	};
	
	DeathCountdown.prototype.type = ComponentType.INTERACTION; // THIS PROPERTY IS REQUIRED FOR ALL COMPONENTS (I miss interfaces)
	DeathCountdown.prototype.name = "DeathCountdown"; // DITTO. THIS PROPERTY IS ALSO REQUIRED
	
	/**
	 * Updates the component. Required for ALL components. Rendering components are special and require ctx as a parameter 
	 * @param {Object} ctx	Canvas drawing object. Only a parameter for RENDERING components
	 */
	DeathCountdown.prototype.update = function(ctx) {
		
		this.countdown--;
		
		if(this.countdown <= 0) {
			// send out message to components that we are dieing
			// hmmm, probably need to implement that message handling stuff
			
			// Kill this entity
			this.entity.destroy();
		}
	};
	
	/**
	 * Starts the component when it is attached to an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	DeathCountdown.prototype.start = function() {
		
	};
	
	/**
	 * Shuts down the component when it is removed from an entity. REQUIRED FOR ALL COMPONENTS 
	 */
	DeathCountdown.prototype.shutdown = function() {
		
	};
	
	/**
	 * Process message received from associated entity 
 	 * @param {Object} message	message to process
	 */
	DeathCountdown.prototype.processMessage = function(message) {
		// do nothing
	};
	
	return DeathCountdown;
});