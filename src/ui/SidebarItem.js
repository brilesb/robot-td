define([
	'jquery'
], function($) {
	
	var SidebarItem = function(name, cost, sprite, createEntity) {
		this.name = name;
		this.cost = cost;
		this.sprite = sprite;
		this.createEntity = createEntity;
		this.ctx = null;
		this.elem = this.createElements();
		this.enabled = true;
		this.sidebar = null;
	};
	
	SidebarItem.prototype.createElements = function() {
		var me = this,
			elem = $("<div class=\"sidebarItem sidebarHover\" width=\"140\" height=\"140\"></div>"),
			canvas = document.createElement("canvas");
		elem.append(canvas);
		
		// setup event listener
		$(elem).click(function () {
			me.onClick();
		});
		
		this.ctx = canvas.getContext("2d");
		
		return elem;
	};
	
	SidebarItem.prototype.draw = function() {
		this.ctx.font="16px Georgia";
	    this.ctx.textAlign = 'center';
		this.ctx.fillText(this.name, 70, 120);
		this.ctx.fillText(this.cost + " Scrap", 70, 135);
	};
	
	SidebarItem.prototype.onClick = function() {
		if(this.sidebar.selectedItem === this) {
			this.sidebar.setSelected(null);
		}
		else {
			this.sidebar.setSelected(this);
		}
		
	};
	
	return SidebarItem;
	
});