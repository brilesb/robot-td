define([
	'jquery',
], function($) {
	
	var GameSidebar = function(items) {
		this.items = items;
		this.selectedItem = null;
		this.addElements();
	};
	
	GameSidebar.prototype.addElements = function() {
		var canvasContainer = $('#canvasContainer'),
			sidebar = $("<div id=\"sidebar\" />");
		sidebar.prependTo(canvasContainer);
		
		// add items to the sidebar.
		// if this game was more complicated it would
		// have a changing item list which would grow as
		// the game progressed. But it's not. :)
		for(var i = 0; i < this.items.length; i++) {
			this.items[i].sidebar = this;
			sidebar.append(this.items[i].elem);
		}
	};
	
	GameSidebar.prototype.removeElements = function() {
		
	};
	
	GameSidebar.prototype.update = function(scrapTotal) {
		// disable/enable side bar items if necessary
		
	};
	
	GameSidebar.prototype.draw = function() {
		for(var i = 0; i < this.items.length; i++) {
			this.items[i].draw();
		}	
	};
	
	GameSidebar.prototype.setSelected = function(item) {
		if(this.selectedItem) {
			$(this.selectedItem.elem).removeClass("sidebarSelected");
			$(this.selectedItem.elem).addClass("sidebarHover");
		}
		if(item) {
			$(item.elem).addClass("sidebarSelected");
			$(item.elem).removeClass("sidebarHover");
		}
		this.selectedItem = item;
	};
	
	return GameSidebar;
	
});