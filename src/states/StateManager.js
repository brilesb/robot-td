define ([], function() {
    /**
     * Manages game states 
     */
    var StateManager = function() {
        this.states = new Array();
    };
    /**
     * Push new state on to the top of the state stack. New state becomes the current state.
     * @param state The state to push on the stack
     */
    StateManager.prototype.pushState = function (state) {
        if (this.states.length > 0) {
            this.states[this.states.length - 1].suspend();
        }
        this.states.push(state);
        state.enter();
    };

    /**
     * Pops a state off the top of the state sack. Next state on the stack becomes the current state.
     */
    StateManager.prototype.popState = function () {
        popped = null;
    			
        if (this.states.length > 0) {
            popped = this.states.pop();
            popped.exit();
            if (this.states.length > 0) {
                this.states[this.states.length - 1].resume();
            }
        }
        return popped;
    };

    /**
     * Updates the current state.
     */
    StateManager.prototype.update = function () {
        if (this.states.length > 0) {
            this.states[this.states.length -1].update();
        }
    };

    /**
     * Draws the current state.
     * @param ctx	Canvas drawing object
     */
    StateManager.prototype.draw = function (ctx, canvasWidth, canvasHeight) {
        if (this.states.length > 0) {
            this.states[this.states.length -1].draw(ctx, canvasWidth, canvasHeight);
        }
    };

    /**
     * Returns the current state 
     */
    StateManager.prototype.getCurrentState = function() {
    	return this.states[this.states.length -1];
    };

    return StateManager;
});
