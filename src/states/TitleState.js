define([
	'jquery',
	'states/MainMenuState'
], function($, MainMenuState) {
	/**
	 * The title screen/intro state
	 * @param manager A reference to the StateManager this state is managed by
	 */
	var TitleState = function (manager) {
	    // Member variables
	    this.manager = manager;
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  The following functions are REQUIRED for every state class
	//-------------------------------------------------------------
	//*************************************************************

	/**
	 * Updates the TitleState
	 */
	TitleState.prototype.update = function() {
	    
	};

	/**
	 * Draws the TitleState
	 * @param ctx   Canvas drawing object
	 */
	TitleState.prototype.draw = function(ctx, canvasWidth, canvasHeight) {
	    // clear the screen!
	    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	    
	    // draw any background stuff
	    
	    // draw logo
	    
	    // draw our click to start text
	    // it'd be cool if this slowly pulsed or something
	    ctx.font="40px Georgia";
	    ctx.textAlign = 'center';
	    ctx.fillText("Click to Start", 480, 320);
	};

	/**
	 * Performs tasks when state is becomes the current state 
	 */
	TitleState.prototype.enter = function () {
		
		// this would be a good place to add event listeners
		var me = this; // keep a local copy of this to pass to event handlers
		$("#gameCanvas").click(function () {
			me.handleClick();
		});
	};

	/**
	 * Performs tasks when this state is exited (popped from state stack) 
	 */
	TitleState.prototype.exit = function () {
		// if you add any event listeners don't forgot to remove them here!
		$("#gameCanvas").unbind("click");
	};

	/**
	 * Performs tasks when another state is pushed on the stack on top of this one 
	 */
	TitleState.prototype.suspend = function () {
		// if you add any event listeners don't forgot to remove them here!
		$("#gameCanvas").unbind("click");
	};

	/**
	 * Performs tasks when a state above this one is popped 
	 */
	TitleState.prototype.resume = function () {
		// need to add back any event listeners... should match the enter function!
		$("#gameCanvas").click(function () {
			this.handleClick(this.manager);
		});
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  End of REQUIRED state functions
	//-------------------------------------------------------------
	//*************************************************************

	TitleState.prototype.handleClick = function() {
		// Go to the next state...
		this.manager.popState();
		this.manager.pushState(new MainMenuState(this.manager));
	};
	
	return TitleState;
});