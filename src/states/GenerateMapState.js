define([
	'constants',
	'states/GameState',
	'maps/TileType',
	'maps/TileMap',
	'assets/AssetManager',
	'maps/GenerateMapEntities'
], function(CONSTANTS, GameState, TileType, TileMap, AssetManager, GenerateMapEntities) {
	/**
	 * The generate map state, procedurally generates map and loads map assets
	 * @param manager A reference to the StateManager this state is managed by
	 */
	var GenerateMapState = function (manager) {
	    // Member variables
	    this.manager = manager;
	    this.tileMap = null;
	    this.gameAssets = new AssetManager();
	    this.entityManager = null;
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  The following functions are REQUIRED for every state class
	//-------------------------------------------------------------
	//*************************************************************

	/**
	 * Updates the state
	 */
	GenerateMapState.prototype.update = function() {
	    // update the progress bar
	};

	/**
	 * Draws the GenerateMapState
	 * @param ctx   Canvas drawing object
	 */
	GenerateMapState.prototype.draw = function(ctx, canvasWidth, canvasHeight) {
	    // clear the screen!
	    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	    
	    // draw any background stuff
	   
	   // draw the progress bar
	   ctx.fillText(175, 400, "Reticulating Splines"); 
	};

	/**
	 * Performs tasks when state is becomes the current state 
	 */
	GenerateMapState.prototype.enter = function () {
		
		var tileTypes = this.loadTileTypes();
		this.tileMap = this.generateTileMap(tileTypes);
		
		var me = this; // I hate javascript event handlers. Please tell me a better way to do this.
		// load tileset, building, unit, and UI sprites
		this.gameAssets.loadImages([{name: "tileset", source:"assets/tilesets/simple.png"}], 
			function () {
				
				me.generateEntities();
				me.onComplete();
			}
		);
	};

	/**
	 * Performs tasks when this state is exited (popped from state stack) 
	 */
	GenerateMapState.prototype.exit = function () {
		
	};

	/**
	 * Performs tasks when another state is pushed on the stack on top of this one 
	 */
	GenerateMapState.prototype.suspend = function () {
		
	};

	/**
	 * Performs tasks when a state above this one is popped 
	 */
	GenerateMapState.prototype.resume = function () {
		
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  End of REQUIRED state functions
	//-------------------------------------------------------------
	//*************************************************************

	GenerateMapState.prototype.loadTileTypes = function() {
		// Ideally this would be loading from a file or something
		// For ease of development this values are hardcoded temporarily
		var types = [];
		
		types[0] = new TileType(1, 0, 0);
		types[1] = new TileType(1, 1, 0);
		types[2] = new TileType(1, 2, 0);
		types[3] = new TileType(1.5, 3, 0);
		types[4] = new TileType(2.5, 4, 0);
		
		return types;
	};

	/**
	 * Generate the tile map. Currently uses the horrendous "random" method.
	 */
	GenerateMapState.prototype.generateTileMap = function(tileTypes) {
		var cols = 40;
		var rows = 30;
		var tileMap = new TileMap(cols, rows, 20, tileTypes);
		
		// Do a really stupid random map generation
		// Need to switch this to something that produces better maps
		for(var i = 0; i < (cols * rows); i++) {
			// this random stuff makes it to hard to see what is going on
			// just set it all to grass right now
			var rand = 0;
			//var rand = Math.floor(Math.random() * (tileTypes.length+10));
			//if (rand < 10) {
			//	rand = 0;
			//}
			//else {
			//	rand = rand - 10;
			//}
			tileMap.tiles[i] = tileTypes[rand];
		}
		
		return tileMap;
	};

	/**
	 * Generates entities that exist at the beginning of the map 
	 */
	GenerateMapState.prototype.generateEntities = function () {
		this.entityManager = GenerateMapEntities.generateEntities();
	};

	GenerateMapState.prototype.onComplete = function() {
		this.manager.popState();
		this.manager.pushState(new GameState(this.manager, this.tileMap, this.gameAssets, this.entityManager));
	};
	
	return GenerateMapState;
});