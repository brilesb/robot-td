define([
	'states/GenerateMapState'
], function(GenerateMapState) {
	/**
	 * The level select state, choose map generation and level options
	 * @param manager A reference to the StateManager this state is managed by
	 */
	var LevelSelectState = function (manager) {
	    // Member variables
	    this.manager = manager;
	    
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  The following functions are REQUIRED for every state class
	//-------------------------------------------------------------
	//*************************************************************

	/**
	 * Updates the state
	 */
	LevelSelectState.prototype.update = function() {
	    
	};

	/**
	 * Draws the LevelSelectState
	 * @param ctx   Canvas drawing object
	 */
	LevelSelectState.prototype.draw = function(ctx, canvasWidth, canvasHeight) {
	    // clear the screen!
	    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	    
	    // draw any background stuff
	    
	};

	/**
	 * Performs tasks when state is becomes the current state 
	 */
	LevelSelectState.prototype.enter = function () {
		//this.createMenuItems();
		
		// For now just skip this state and go straight to the map generation state
		this.manager.popState();
		this.manager.pushState(new GenerateMapState(this.manager));
	};

	/**
	 * Performs tasks when this state is exited (popped from state stack) 
	 */
	LevelSelectState.prototype.exit = function () {
		//this.removeMenuItems();
		
		
	};

	/**
	 * Performs tasks when another state is pushed on the stack on top of this one 
	 */
	LevelSelectState.prototype.suspend = function () {
		
	};

	/**
	 * Performs tasks when a state above this one is popped 
	 */
	LevelSelectState.prototype.resume = function () {
		
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  End of REQUIRED state functions
	//-------------------------------------------------------------
	//*************************************************************

	LevelSelectState.prototype.createMenuItems = function () {
		
	};

	LevelSelectState.prototype.removeMenuItems = function () {

	};

	return LevelSelectState;
});