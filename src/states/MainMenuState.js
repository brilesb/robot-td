define([
	'jquery',
	'states/LevelSelectState'
], function($, LevelSelectState) {
	/**
	 * The main menu state
	 * @param manager A reference to the StateManager this state is managed by
	 */
	var MainMenuState = function (manager) {
	    // Member variables
	    this.manager = manager;
	    
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  The following functions are REQUIRED for every state class
	//-------------------------------------------------------------
	//*************************************************************

	/**
	 * Updates the state
	 */
	MainMenuState.prototype.update = function() {
	    
	};

	/**
	 * Draws the MainMenuState
	 * @param ctx   Canvas drawing object
	 */
	MainMenuState.prototype.draw = function(ctx, canvasWidth, canvasHeight) {
	    // clear the screen!
	    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	    
	    // draw any background stuff
	    
	    // draw small logo
	    
	};

	/**
	 * Performs tasks when state is becomes the current state 
	 */
	MainMenuState.prototype.enter = function () {
		this.createMenuItems();
	};

	/**
	 * Performs tasks when this state is exited (popped from state stack) 
	 */
	MainMenuState.prototype.exit = function () {
		this.removeMenuItems();
	};

	/**
	 * Performs tasks when another state is pushed on the stack on top of this one 
	 */
	MainMenuState.prototype.suspend = function () {
		
	};

	/**
	 * Performs tasks when a state above this one is popped 
	 */
	MainMenuState.prototype.resume = function () {
		
	};

	//*************************************************************
	//-------------------------------------------------------------
	//  End of REQUIRED state functions
	//-------------------------------------------------------------
	//*************************************************************

	MainMenuState.prototype.createMenuItems = function () {
		var me = this,
			canvasContainer = $('#canvasContainer'),
			mItem;

		mItem = $("<a id=\"startItem\" class=\"menuItem menuPos1\" href=\"#\">Start New Game</a>");
		mItem.on('click', function() {
			me.handleStartClick();
			return false;
		}).prependTo(canvasContainer);
		
		mItem = $("<a id=\"optionsItem\" class=\"menuItem menuPos2\" href=\"#\">Options</a>");
		mItem.on('click', function() {
			me.handleOptionsClick();
			return false;
		}).prependTo(canvasContainer);
	};

	MainMenuState.prototype.removeMenuItems = function () {
		$("#startItem").remove();
		$("#optionsItem").remove();
	};

	MainMenuState.prototype.handleStartClick = function() {
		
		// Go to the level select state
		// ideally we should leave this state on the stack so that the user can
		// come back to the main menu from the level select state, but for now
		// because the level select goes straight through to the map generation state
		// we will just kill the main menu state here
		this.manager.popState();
		this.manager.pushState(new LevelSelectState(this.manager));
	};

	MainMenuState.prototype.handleOptionsClick = function() {
		alert("OPTIONS HAS BEEN CLICKED");
	};

	return MainMenuState;
});