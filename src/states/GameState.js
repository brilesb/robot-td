define([
	'constants',
	'assets/Sprite',
	'maps/TileMapRenderer',
	'ui/GameSidebar',
	'ui/SidebarItem',
	'entities/EntityManager',
	'entities/Entity',
	'geometry/vector',
	'Team',
	'components/ComponentType',
	'components/BlockBuilding',
	'components/Damageable',
	'components/ProjEmitter',
	'components/TowerAI',
	'components/DebugRender',
	'components/RotateToTarget',
	'components/Damaging',
	'components/MovingPhysics',
	'components/InstantDamager',
	'entities/EnemyGenerator'
], function(CONSTANTS, Sprite,TileMapRenderer,GameSidebar,SidebarItem,EntityManager,Entity,Vector,Team,ComponentType,BlockBuilding,
			Damageable,ProjEmitter,TowerAI,DebugRender,RotateToTarget,Damaging,MovingPhysics,InstantDamager,EnemyGenerator) {
    /**
     * The game state, for actually playing the game
     * @param manager A reference to the StateManager this state is managed by
     */
    var GameState = function (manager, tileMap, gameAssets, entityManager) {
        // Member variables
        this.manager = manager;
        this.tileMap = tileMap;
        this.gameAssets = gameAssets;
        this.tileMapRenderer = null;
        this.entityManager = entityManager;
        this.sidebar = null;
        this.enemyGenerator = new EnemyGenerator(entityManager);
        
        this.scrapTotal = 10000;
    };

    //*************************************************************
    //-------------------------------------------------------------
    //  The following functions are REQUIRED for every state class
    //-------------------------------------------------------------
    //*************************************************************

    /**
     * Updates the state
     */
    GameState.prototype.update = function() {
        // update entity state
        this.entityManager.update();
        this.enemyGenerator.update();
        this.sidebar.update(this.scrapTotal);
    };

    /**
     * Draws the GameState
     * @param ctx   Canvas drawing object
     */
    GameState.prototype.draw = function(ctx, canvasWidth, canvasHeight) {
        // draw any background stuff
        
        // draw the tile map
        this.tileMapRenderer.drawBufferedMap(ctx, CONSTANTS.MAP_OFFSET.x, CONSTANTS.MAP_OFFSET.y);
        
        // draw entities
        this.entityManager.draw(ctx);
        
        // draw UI

    };

    /**
     * Performs tasks when state is becomes the current state 
     */
    GameState.prototype.enter = function () {
		// setup tile map renderer
		this.tileMapRenderer = new TileMapRenderer(new Sprite(this.gameAssets.images["tileset"]), this.tileMap.numColumns*this.tileMap.tileSize, this.tileMap.numRows*this.tileMap.tileSize);
		this.tileMapRenderer.drawMapToBuffer(this.tileMap);

		// setup sidebar
		// hard-coded sidebar items (this is lame please change)
		// need to move this out of the game state and into somewhere else
		// I am taking suggestions on where to move this, help me!
		var items = [];
		var createFunc = function() { // wall create function
			var entity = new Entity();
			entity.position = new Vector(0,0); // entity has these properties but I don't know what they are yet
			entity.width = 20; // I know these ones though
			entity.height = 20;
			entity.team = Team.PLAYER;
			entity.attachComponent(new DebugRender("#0077FF", CONSTANTS.MAP_OFFSET));
			entity.attachComponent(new BlockBuilding());
			entity.attachComponent(new Damageable(50)); // this number is made up
			return entity;
		};
		items.push(new SidebarItem("Wall",50,new Sprite(this.gameAssets.images["ui"],0,0,20,20), createFunc));
		
		createFunc = function() { // missile tower create function
			var projCreate = function() {
				var entity = new Entity();
				entity.position = new Vector(0,0);
				entity.width = 6;
				entity.height = 6;
				entity.team = Team.PLAYER;
				entity.attachComponent(new DebugRender("#225533", CONSTANTS.MAP_OFFSET));
				entity.attachComponent(new Damaging(2));
				entity.attachComponent(new MovingPhysics(2.5));
				
				entity.debugMe = true;
				
				return entity;
			};
			
			var entity = new Entity();
			entity.position = new Vector(0,0); // entity has these properties but I don't know what they are yet
			entity.width = 20; // I know these ones though
			entity.height = 20;
			entity.target = null; // no target to start with
			entity.lookDirection = new Vector(1,0); // everybody look to the right
			entity.team = Team.PLAYER;
			entity.attachComponent(new DebugRender("#443388", CONSTANTS.MAP_OFFSET));
			entity.attachComponent(new BlockBuilding());
			entity.attachComponent(new Damageable(100)); // this number is made up
			entity.attachComponent(new ProjEmitter(this.entityManager, 20, projCreate));
			entity.attachComponent(new TowerAI(40));
			entity.attachComponent(new RotateToTarget(2));
			return entity;
		};
		items.push(new SidebarItem("Missile Cannon",250,new Sprite(this.gameAssets.images["ui"],20,0,20,20), createFunc));
		
		createFunc = function() { // laser tower create function
			var entity = new Entity();
			entity.position = new Vector(0,0); // entity has these properties but I don't know what they are yet
			entity.width = 20; // I know these ones though
			entity.height = 20;
			entity.target = null; // no target to start with
			entity.lookDirection = new Vector(1,0); // everybody look to the right
			entity.team = Team.PLAYER;
			entity.attachComponent(new DebugRender("#443388", CONSTANTS.MAP_OFFSET));
			entity.attachComponent(new BlockBuilding());
			entity.attachComponent(new Damageable(100)); // this number is made up
			entity.attachComponent(new InstantDamager (20, 4));
			entity.attachComponent(new TowerAI(40));
			entity.attachComponent(new RotateToTarget(2));
			return entity;
		};
		items.push(new SidebarItem("Laser Cannon",150,new Sprite(this.gameAssets.images["ui"],20,0,20,20), createFunc));
		
		this.sidebar = new GameSidebar(items);
		this.sidebar.draw();
		
		this.enemyGenerator.startNextWave();
		
		// add eventListeners
		this.addEventListeners();
    };

    /**
     * Performs tasks when this state is exited (popped from state stack) 
     */
    GameState.prototype.exit = function () {
    	this.removeEventListeners();
    };

    /**
     * Performs tasks when another state is pushed on the stack on top of this one 
     */
    GameState.prototype.suspend = function () {
    	this.removeEventListeners();
    };

    /**
     * Performs tasks when a state above this one is popped 
     */
    GameState.prototype.resume = function () {
    	this.addEventListeners();
    };

    //*************************************************************
    //-------------------------------------------------------------
    //  End of REQUIRED state functions
    //-------------------------------------------------------------
    //*************************************************************

	GameState.prototype.addEventListeners = function() {
		var me = this,
			canvas = $("#gameCanvas"); // wish I didn't have to specify this in a bunch of files. Somebody should fix that.
		
		canvas.click(function (e) {
			// this should go in a helper function or something
			var x = e.pageX - $(canvas).offset().left,
            	y = e.pageY - $(canvas).offset().top;
			me.onClick(x, y);
		});
	};
	
	GameState.prototype.removeEventListeners = function() {
		$("#gameCanvas").unbind(); // boom! all gone
	};

	GameState.prototype.onClick = function(x, y) {
		var mapX = x - CONSTANTS.MAP_OFFSET.x,
			mapY = y - CONSTANTS.MAP_OFFSET.y;
		if(this.sidebar.selectedItem) {
			this.placeSelectedBuilding(mapX, mapY);
		}
	};

	/**
	 * Attempt to place the currently selected building.
	 * @param {Object} x	X position (relative to game map)
	 * @param {Object} y	Y position (relateive to game map)
	 */
	GameState.prototype.placeSelectedBuilding = function(x, y) {
		// get the row/column of the mouse coordinates
		var tilePos = this.tileMap.getTilePosAtXY(x, y);
		var tileX = tilePos.column * this.tileMap.tileSize + this.tileMap.tileSize/2;
		var tileY = tilePos.row * this.tileMap.tileSize + this.tileMap.tileSize/2;
		
		// check to see if there any blocking entities at that location
		var blocking = this.entityManager.getEntitiesWithComponentInArea(ComponentType.STATIC,"BlockBuilding",tileX,tileY,this.tileMap.tileSize,this.tileMap.tileSize);
		
		if(blocking.length == 0) {
			// create and add entity
			var entity = this.sidebar.selectedItem.createEntity();
			entity.position = new Vector(tileX, tileY);
			this.entityManager.addEntity(entity);
		}
		else
			alert("Placement at ("+tileX+","+tileY+") blocked by entity at ("+blocking[0].position.x+","+blocking[0].position.y+") "+blocking[0].width+"x"+blocking[0].height);
		
	};
	
    return GameState;
});