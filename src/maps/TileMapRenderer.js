define([], function() {
	/**
	 * Renders a tile map 
	 */
	var TileMapRenderer = function(tileset, mapWidth, mapHeight) {
		this.tileset = tileset;
		this.initBuffer(mapWidth, mapHeight);
	};
	
	/**
	 * Initializes an off-screen canvas buffer we can draw the map tiles to
	 * @param {Object} mapWidth		Width of map (pixels)
	 * @param {Object} mapHeight	Height of map (pixels)
	 */
	TileMapRenderer.prototype.initBuffer = function(mapWidth, mapHeight) {
		this.buffer = document.createElement('canvas');
		this.buffer.width = mapWidth;
		this.buffer.height = mapHeight;
	};

	/**
	 * Draws a tileMap to the off-screen buffer.
 	 * @param {Object} tileMap	Tilemap to draw
	 */
	TileMapRenderer.prototype.drawMapToBuffer = function(tileMap) {
		var ctx = this.buffer.getContext('2d');
		var tileSize = tileMap.tileSize;
		var tileType;
		
		var index = 0;
		for (var r = 0; r < tileMap.numRows; r++) {
			for (var c = 0; c < tileMap.numColumns; c++) {
				tileType = tileMap.tiles[index];
				this.tileset.draw(ctx, c*tileSize, r*tileSize, tileType.spriteCol*tileSize, tileType.spriteRow*tileSize, tileSize, tileSize);
				
				index++;
			}
		}
	};
	
	/**
	 * Draw the off-screen buffer to the main canvas. This allows us to not have to draw individual tiles 60x a second. 
	 * @param {Object} x
	 * @param {Object} y
	 * @param {Object} ctx
	 */
	TileMapRenderer.prototype.drawBufferedMap = function(ctx, x, y) {
		ctx.drawImage(this.buffer, x, y);
	};
	
	TileMapRenderer.prototype.destroy = function() {
		this.buffer = null;
		this.tileset = null;
	};

	return TileMapRenderer;
});