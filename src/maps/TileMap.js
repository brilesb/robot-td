define([], function() {
	/**
	 * A game map, made up of a 2D matrix of tiles 
	 */
	var TileMap = function(columns, rows, tileSize, tileTypes) {
		
		this.tiles = [columns * rows]; // 2D matrix of references to tile types
		this.numColumns = columns;
		this.numRows = rows;
		this.tileSize = tileSize; // only allows square tiles, no rectangle crap
		this.tileTypes = tileTypes; // an array of TileType objects
	};

	/**
	 * Returns the type of tile at a column and row position 
	 * @param {Object} col	column in the tilemap
	 * @param {Object} row 	row in the tilemap
	 */
	TileMap.prototype.getTile = function(col, row) {
		return this.tiles[this.numColumns * row + col];
	};

	/**
	 * Returns the type of tile at an (x, y) position
	 * @param {Object} x	X position
	 * @param {Object} y	Y position
	 */
	TileMap.prototype.getTileAtXY = function(x, y) {
		var tilePos = this.getTilePosAtXY(x, y);
		
		return this.getTile(tilePos.column, tilePos.row);
	};
	
	TileMap.prototype.getTilePosAtXY = function(x,y) {
		var tilePos = { column: 0, row: 0 };
		
		if (x) {
			tilePos.column = Math.floor(x / this.tileSize);
		}
		if (y) {
			tilePos.row = Math.floor(y / this.tileSize);
		}
		
		return tilePos;
	};

	return TileMap;
});