define([], function() {
	var TileType = function(terrainWeight, spriteCol, spriteRow) {
		this.spriteCol = spriteCol;
		this.spriteRow = spriteRow;
		this.terrainWeight = terrainWeight;
	};

	return TileType;
});