define([
	'constants',
	'geometry/vector',
	'entities/EntityManager',
	'entities/Entity',
	'Team',
	'components/DebugRender',
	'components/BlockBuilding',
	'components/Damageable',
	'components/GoalTarget'
], function(CONSTANTS, Vector, EntityManager, Entity, Team, DebugRender, BlockBuilding, Damageable, GoalTarget) {
	
	var generateEntities = function() {
		var entityManager = new EntityManager();
		
		generateRequired(entityManager);
		
		return entityManager;
	};
	
	var generateRequired  = function(entityManager) {
		// this is an over-simplified method of creating required entities
		// need to switch this over to something more sophisticated
		var entity = new Entity();
		entity.position = new Vector(130, 270);
		entity.width = 20;
		entity.height = 20;
		entity.team = Team.PLAYER;
		entity.attachComponent(new DebugRender("#880055", CONSTANTS.MAP_OFFSET));
		entity.attachComponent(new BlockBuilding());
		entity.attachComponent(new Damageable(150)); // this number is made up
		entity.attachComponent(new GoalTarget());
		entityManager.addEntity(entity);
		
		entity = new Entity();
		entity.position = new Vector(200, 260);
		entity.width = 40;
		entity.height = 40;
		entity.team = Team.PLAYER;
		entity.attachComponent(new DebugRender("#880055", CONSTANTS.MAP_OFFSET));
		entity.attachComponent(new BlockBuilding());
		entity.attachComponent(new Damageable(200)); // this number is made up
		entity.attachComponent(new GoalTarget());
		entityManager.addEntity(entity);
	};
	
	return {
		generateEntities: generateEntities,
		generateRequired: generateRequired
	};
});