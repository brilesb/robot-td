define([], function() {
	
	var Entity = function() {
		this.manager = null;
		this.components = {}; // List of components, indexed by name
	};

	/**
	 * Attaches a new component to this Entity. If a property of the same name exists, it will replace that property.
 	 * @param {Object} component	Component to attach
	 */
	Entity.prototype.attachComponent = function(component) {
		this.components[component.name] = component;
		component.entity = this;
		component.start();
	};

	/**
	 * Detaches a component with a specific name from this Entity
	 * @param {Object} component	Name of the component to remove
	 */
	Entity.prototype.removeComponentByName = function(name) {
		var component = this.components[name];
		if (component) {
			component.shutdown();
			delete this.components[name];
		}
	};
	
	/**
	 * Destroys this Entity and shuts down all its components 
	 */
	Entity.prototype.destroy = function() {
		// remove the entity from the manager if it has been
		// added to a manager
		if (this.manager != null) {
			this.manager.removeEntity(this);
			this.manager = null;
		}
		
		for (compName in this.components) {
			this.components[compName].shutdown();
		}
		this.components = null;
	};
	
	/**
	 * Returns true if this entity has a specific component
	 * @param {Object} name Name of the component to check for 
	 */
	Entity.prototype.hasComponent = function(name) {
		return this.components.hasOwnProperty(name);
	};

	/**
	 * Sends a message to all components attached to thie entity 
     * @param {Object} message
	 */
	Entity.prototype.sendMessage = function(message) {
		for (compName in this.components) {
			this.components[compName].processMessage(message);
		}
	};
	
	return Entity;
});