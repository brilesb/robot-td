define([
	'components/ComponentType'
], function(ComponentType) {
	var EntityManager = function() {
		this.entities = [];
		this.components = {};
	};

	/**
	 * Update all managed entities 
	 */
	EntityManager.prototype.update = function() {
		var i;
		// update AI
		if(this.components[ComponentType.AI] != null) {
			for(i = 0; i < this.components[ComponentType.AI].length; i++) {
				this.components[ComponentType.AI][i].update();
			}	
		}	
		// update Physics
		if(this.components[ComponentType.PHYSICS] != null) {
			for(i = 0; i < this.components[ComponentType.PHYSICS].length; i++) {
				this.components[ComponentType.PHYSICS][i].update();
			}	
		}
		// update Physics Post Processing
		if(this.components[ComponentType.POST_PHYSICS] != null) {
			for(i = 0; i < this.components[ComponentType.POST_PHYSICS].length; i++) {
				this.components[ComponentType.POST_PHYSICS][i].update();
			}	
		}
		// update Interaction
		if(this.components[ComponentType.INTERACTION] != null) {
			for(i = 0; i < this.components[ComponentType.INTERACTION].length; i++) {
				this.components[ComponentType.INTERACTION][i].update();
			}	
		}
		
	};

	/**
	 * Draw all managed entities 
	 * @param {Object} ctx	Canvas drawing object
	 */
	EntityManager.prototype.draw = function(ctx) {
		// update Rendering
		if(this.components[ComponentType.RENDERING] != null) {
			for(i = 0; i < this.components[ComponentType.RENDERING].length; i++) {
				this.components[ComponentType.RENDERING][i].update(ctx); // rendering components don't fit the no parameters pattern
			}	
		}
	};

	/**
	 * Add a new entity to be managed 
	 * @param {Object} entity	Entity to add
	 */
	EntityManager.prototype.addEntity = function(entity) {
		this.entities.push(entity);
		entity.manager = this;
		// register all of its components
		for(compName in entity.components) { // components is an object
			this.registerComponent(entity.components[compName]);
		}
		
	};

	/**
	 * Remove an entity from the manager 
	 * @param {Object} entity	Entity to remove
	 */
	EntityManager.prototype.removeEntity = function(entity) {
		for(var i = 0; i < this.entities.length; i++) {
			if(this.entities[i] === entity) {
				for(compName in this.entities[i].components) {
					this.unregisterComponent(this.entities[i].components[compName]);
				}
				this.entities.splice(i,1);
				break;
			}
		}
	};
	
	/**
	 * Registers a component with the EntityManager so that it actually gets updated
	 */
	EntityManager.prototype.registerComponent = function(component) {
		if(!this.components[component.type]) {
			this.components[component.type] = [];
		}
		this.components[component.type].push(component);
	};

	/**
	 * Unregisters a component with the EntityManager so it no longer gets updated 
	 */
	EntityManager.prototype.unregisterComponent = function(component) {
		if(!this.components[component.type]) {
			for(var i = 0; i < this.components[component.type].length; i++) {
				if(this.components[component.type][i] === component) {
					this.components[component.type].splice(i,1);
					break;
				}
			}
		}
	};
	
	/**
	 * Get an array of all entities in an rectangluar area
	 * @param {Object} x	X-coordinate of upper left point of area
	 * @param {Object} y	Y-coordinate of upper left point of area
	 * @param {Object} width	Width of area
	 * @param {Object} height	Height of area
	 */
	EntityManager.prototype.getEntitiesInArea = function(x, y, width, height) {
		var areaEntities = [];
		for(var i = 0; i < this.entities.length; i++) {
			var entity = this.entities[i];
			// only look at properties with position, width, and height properties
			if(entity.width && entity.height && entity.position) {
				if (  !(x + width-1 < entity.position.x ||
						y + height-1 < entity.position.y ||
						x > entity.x + entity.width-1 ||
						y > entity.y + entity.height-1) ) {
					areaEntities.push(entity);			
				}
			}
		}
		return areaEntities;
	};
	
	/**
	 * Get an array of entities with a specific attachedcomponent in a rectangluar area
	 * @param {Object} compType	Type of component
	 * @param {Object} compName	Name of component
	 * @param {Object} x		X-coordinate of upper left point of area
	 * @param {Object} y		Y-coordinate of upper left point of area
	 * @param {Object} width	Width of area
	 * @param {Object} height	Height of area
	 */
	EntityManager.prototype.getEntitiesWithComponentInArea = function(compType, compName, x, y, width, height) {
		var searchList = this.components[compType];
		var areaEntities = [];
		for(var i = 0; i < searchList.length; i++) {
			var entity = searchList[i].entity;
			if(entity.hasComponent(compName)) {
				// check to see if it is in area
				if(entity.width && entity.height && entity.position) {
					if (  !(x + width-1 < entity.position.x ||
							y + height-1 < entity.position.y ||
							x > entity.position.x + entity.width-1 ||
							y > entity.position.y + entity.height-1) ) {
						areaEntities.push(entity);
					}
				}
			}
		}
		return areaEntities;	
	};
	
	/**
	 * Gets an array of entites (sorted by distance) with a specific attached component within a certain distance of a point
	 * @param {Object} compType	Type of component
	 * @param {Object} compName	Name of component
	 * @param {Object} point	Center point of range
	 * @param {Object} range 	Distance from center to find entities
	 * @return {Object} Returns array of objects containing the properties distanceSqr and entity. Sorted by distanceSqr ascending.
	 */
	EntityManager.prototype.getEntitiesWithComponentInRange = function(compType, compName, point, range) {
		var rangeSqr = range * range;
		var searchList = this.components[compType];
		var areaEntities = [];
		var entityDistSqr;
		for(var i = 0; i < searchList.length; i++) {
			var entity = searchList[i].entity;
			if(entity.hasComponent(compName)) {
				// check to see if it is within range
				if(entity.position) {
					entityDistSqr = point.distanceToSquared(entity.position);
					if ( entityDistSqr <= rangeSqr ) {
						areaEntities.push({ distanceSqr: entityDistSqr, entity: entity });
					}
				}
			}
		}
		// sort by distance
		if(areaEntities.length > 1) {
			areaEntities.sort(function (a, b) {
			    if (a.distanceSqr > b.distanceSqr)
			      return 1;
			    if (a.distanceSqr < b.distanceSqr)
			      return -1;
			    return 0;
			});
		}
		return areaEntities;	
	};
	
	return EntityManager;
});