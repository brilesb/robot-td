define([
	'entities/EnemyWave',
	'geometry/vector'
], function(EnemyWave,Vector) {
	var EnemyGenerator = function(entityManager) {
		this.entityManager = entityManager;
		this.waves = [];
		this.currentWave = null;
		this.initWaves();
	};
	
	EnemyGenerator.prototype.initWaves = function() {
		// just create a wave to use for testing
		var wave = new EnemyWave(this.entityManager, 50, 750, 500, new Vector(-1, 0));
		
		this.waves.push(wave);
	};

	/**
	 * Update current wave;
	 */
	EnemyGenerator.prototype.update = function() {
		if(this.currentWave) {
			this.currentWave.update();
		}
	};
	
	EnemyGenerator.prototype.startNextWave = function() {
		if(this.currentWave) {
			this.currentWave.stop();
		}
		if(this.waves.length > 0) {
			this.currentWave = this.waves.shift();
			this.currentWave.start();
		}
	};

	
	return EnemyGenerator;
});