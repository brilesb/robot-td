define([
	'constants',
	'geometry/vector',
	'entities/Entity',
	'Team',
	'components/Damageable',
	'components/DebugRender',
	'components/MovingPhysics',
	'components/RotateToTarget',
	'components/ProjEmitter',
	'components/WalkerAI'
], function(CONSTANTS,Vector,Entity,Team,Damageable,DebugRender,MovingPhysics,RotateToTarget,ProjEmitter,WalkerAI) {
	var EnemyWave = function(entityManager, timeBetweenEnemies, spawnX, spawnY, initDir) {
		this.entityManager = entityManager;
		this.timeBetween = timeBetweenEnemies;
		this.spawnX = spawnX;
		this.spawnY = spawnY;
		this.initDir = initDir;
		this.entities = [];
		this.counter = 0;
		
		this.init();
	};
	
	EnemyWave.prototype.init = function() {
		// just create a bunch of enemies for testing
		for(var i = 0; i < 10; i++) {
			var entity = new Entity();
			entity.position = new Vector(0,0);
			entity.width = 10;
			entity.height = 10;
			entity.lookDirection = new Vector(this.initDir.x, this.initDir.y);
			entity.team = Team.HOSTILE;
			entity.attachComponent(new DebugRender("#FF00FF", CONSTANTS.MAP_OFFSET));
			entity.attachComponent(new Damageable(10)); // this number is made up
			entity.attachComponent(new ProjEmitter(this.entityManager, 2, 5, null));
			entity.attachComponent(new WalkerAI());
			entity.attachComponent(new RotateToTarget(3));
			entity.attachComponent(new MovingPhysics(1));
			this.entities.push(entity);
		}
	};

	/**
	 * Update the wave.
	 */
	EnemyWave.prototype.update = function() {
		if(this.entities.length > 0) {
			this.counter--;
			
			// is it time to release a new entity?
			if(this.counter <= 0) {
				var entity = this.entities.shift();
				entity.LOOK_AT_ME = true;	
				entity.position.set(this.spawnX,this.spawnY);
				this.entityManager.addEntity(entity);
				
				this.counter = this.timeBetween;
			}
		}
	};
	
	EnemyWave.prototype.start = function() {
		
	};

	EnemyWave.prototype.stop = function() {
		this.entities = null;	
	};
	
	return EnemyWave;
});